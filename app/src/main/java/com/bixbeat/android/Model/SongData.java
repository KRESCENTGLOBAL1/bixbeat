package com.bixbeat.android.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SongData {

    @SerializedName("song")
    @Expose
    private Song song;
    @SerializedName("songs")
    @Expose
    private ArrayList<Song_> songs = null;

    public Song getSong() {
        return song;
    }

    public void setSong(Song song) {
        this.song = song;
    }

    public ArrayList<Song_> getSongs() {
        return songs;
    }

    public void setSongs(ArrayList<Song_> songs) {
        this.songs = songs;
    }
}
