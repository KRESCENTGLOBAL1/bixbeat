package com.bixbeat.android.Model;

import com.bixbeat.android.roomdatabase.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Loggeddata {
    @SerializedName("user")
    @Expose
    private AccUser accuser;
    @SerializedName("state")
    @Expose
    private Boolean state;

    public AccUser getAccuser() {
        return accuser;
    }

    public void setAccuser(AccUser accuser) {
        this.accuser = accuser;
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

}
