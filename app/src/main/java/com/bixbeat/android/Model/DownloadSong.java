package com.bixbeat.android.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DownloadSong {

    @SerializedName("musicId")
    @Expose
    private Integer musicId;
    @SerializedName("uId")
    @Expose
    private String uId;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("absoluteAudioPath")
    @Expose
    private String absoluteAudioPath;
    @SerializedName("playPathAbsolute")
    @Expose
    private String playPathAbsolute;
    @SerializedName("downloadPathAbsolute")
    @Expose
    private String downloadPathAbsolute;
    @SerializedName("songName")
    @Expose
    private String songName;
    @SerializedName("artistName")
    @Expose
    private String artistName;
    @SerializedName("featuringName")
    @Expose
    private Object featuringName;
    @SerializedName("genre")
    @Expose
    private Object genre;
    @SerializedName("countryName")
    @Expose
    private String countryName;
    @SerializedName("imageSmSrc")
    @Expose
    private String imageSmSrc;
    @SerializedName("imageLgSrc")
    @Expose
    private String imageLgSrc;
    @SerializedName("fileName")
    @Expose
    private String fileName;

    public Integer getMusicId() {
        return musicId;
    }

    public void setMusicId(Integer musicId) {
        this.musicId = musicId;
    }

    public String getUId() {
        return uId;
    }

    public void setUId(String uId) {
        this.uId = uId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAbsoluteAudioPath() {
        return absoluteAudioPath;
    }

    public void setAbsoluteAudioPath(String absoluteAudioPath) {
        this.absoluteAudioPath = absoluteAudioPath;
    }

    public String getPlayPathAbsolute() {
        return playPathAbsolute;
    }

    public void setPlayPathAbsolute(String playPathAbsolute) {
        this.playPathAbsolute = playPathAbsolute;
    }

    public String getDownloadPathAbsolute() {
        return downloadPathAbsolute;
    }

    public void setDownloadPathAbsolute(String downloadPathAbsolute) {
        this.downloadPathAbsolute = downloadPathAbsolute;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public Object getFeaturingName() {
        return featuringName;
    }

    public void setFeaturingName(Object featuringName) {
        this.featuringName = featuringName;
    }

    public Object getGenre() {
        return genre;
    }

    public void setGenre(Object genre) {
        this.genre = genre;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getImageSmSrc() {
        return imageSmSrc;
    }

    public void setImageSmSrc(String imageSmSrc) {
        this.imageSmSrc = imageSmSrc;
    }

    public String getImageLgSrc() {
        return imageLgSrc;
    }

    public void setImageLgSrc(String imageLgSrc) {
        this.imageLgSrc = imageLgSrc;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

}
