package com.bixbeat.android.roomdatabase;

import android.media.MediaPlayer;

public class MediaHelper {
    private static MediaHelper instance;
    private MediaPlayer mediaPlayer;

    private MediaHelper() {
    }

    public static MediaHelper getInstance() {
        if (instance == null){
            instance = new MediaHelper();
        }
        return instance;
    }

    public void setMediaPlayer(MediaPlayer mediaPlayer) {
        this.mediaPlayer = mediaPlayer;
    }

    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }
}
