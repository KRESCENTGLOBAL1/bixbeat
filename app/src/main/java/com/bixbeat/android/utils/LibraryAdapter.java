package com.bixbeat.android.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bixbeat.android.MainActivity;
import com.bixbeat.android.R;
import com.bixbeat.android.roomdatabase.User;
import java.util.List;
import androidx.recyclerview.widget.RecyclerView;

    public class LibraryAdapter extends RecyclerView.Adapter<LibraryAdapter.MyViewHolder> {

        private Context context;
        private List<User> data;
        private static final String TAG = "LibraryAdapter";

        public LibraryAdapter(Context contexts, List<User> data) {

            this.context = contexts;
            this.data = data;
        }

        @Override
        public LibraryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_layout, parent, false);
            return new LibraryAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final LibraryAdapter.MyViewHolder holder, final int position) {

            holder.song_title.setText(data.get(position).song_name);
            holder.txt_artistname.setText(data.get(position).artist_name);

            holder.ll_chatcard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((MainActivity)context).localservice(data.get(position),position,true);
                }
            });
        }

        @Override
        public int getItemCount() {

            return data.size();
        }

        public void update(Context context, List<User> users) {
            this.context = context;
            this.data = users;

            notifyDataSetChanged();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder{

            TextView txt_artistname,song_title;
            LinearLayout ll_chatcard;

            public MyViewHolder(View view) {
                super(view);

                song_title=view.findViewById(R.id.song_title);
                txt_artistname=view.findViewById(R.id.txt_artistname);
                ll_chatcard=view.findViewById(R.id.ll_messages_card);
            }
            }
        }



