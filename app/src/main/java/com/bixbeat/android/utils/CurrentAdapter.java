package com.bixbeat.android.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;


import com.bixbeat.android.MainActivity;
import com.bixbeat.android.Model.Song_;
import com.bixbeat.android.R;

import java.util.ArrayList;

public class CurrentAdapter extends RecyclerView.Adapter<CurrentAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<Song_> data;
    private static final String TAG = "CurrentAdapter";

    public CurrentAdapter(Context contexts, ArrayList<Song_> data) {

        this.context = contexts;
        this.data = data;
    }

    @Override
    public CurrentAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_layout, parent, false);
        return new CurrentAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CurrentAdapter.MyViewHolder holder, final int position) {

        holder.song_title.setText(data.get(position).getSongName());
        holder.txt_artistname.setText(data.get(position).getArtistName());

        holder.ll_chatcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)context).playmusiconline(data.get(position), true);
            }
        });
    }

    @Override
    public int getItemCount() {

        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView txt_artistname,song_title;
        LinearLayout ll_chatcard;

        public MyViewHolder(View view) {
            super(view);

            song_title=view.findViewById(R.id.song_title);
            txt_artistname=view.findViewById(R.id.txt_artistname);
            ll_chatcard=view.findViewById(R.id.ll_messages_card);
        }
    }
}



