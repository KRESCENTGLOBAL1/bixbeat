package com.bixbeat.android.utils;

import android.app.Application;

import com.flurry.android.FlurryAgent;

public class MyApplication extends Application
{
        @Override
        public void onCreate() {
            super.onCreate();
            new FlurryAgent.Builder()
                    .withLogEnabled(true)
                    .build(this, "BSCFXPTH3B7NRZ88BTKK");
        }
}