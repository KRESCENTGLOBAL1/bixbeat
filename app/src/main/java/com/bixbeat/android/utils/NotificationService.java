package com.bixbeat.android.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;
import android.widget.RemoteViews;

import com.bixbeat.android.MainActivity;

import com.bixbeat.android.R;
import com.bixbeat.android.roomdatabase.User;

import androidx.annotation.RequiresApi;

public class NotificationService extends Service {

    Notification status;
    private final String LOG_TAG = "NotificationService";
    static MediaPlayer mp;
    static User userr;
    static Context context;
    RemoteViews views;
    String change = "pause";
    Intent notificationIntent;
    PendingIntent pendingIntent;
    NotificationManager manager;
    NotificationChannel channel;
    static String image, songname, artistname, localmusicpath;

    public static void notdata(String imageSmSrc, String artistName, String songName)
    {
        image = imageSmSrc;
        songname =songName;
        artistname=artistName;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null && !TextUtils.isEmpty(intent.getAction()) && intent.getAction().equals(Constants.ACTION.STARTFOREGROUND_ACTION)) {
            showNotification(change);
           // Toast.makeText(this, "Service Started", Toast.LENGTH_SHORT).show();

        } else if (intent.getAction().equals(Constants.ACTION.PREV_ACTION)) {
          //  Toast.makeText(this, "Clicked Previous", Toast.LENGTH_SHORT).show();
            Log.i(LOG_TAG, "Clicked Previous");

            ((MainActivity) context).playprev();
        }
        else if (intent.getAction().equals(Constants.ACTION.setdata))
        {
         // bigViews.setImageViewResource(R.id.status_bar_play, R.drawable.pause);
         // views.setImageViewResource(R.id.status_bar_play, R.drawable.pause);

            views.setTextViewText(R.id.status_bar_track_name, ""+songname);
            views.setTextViewText(R.id.status_bar_artist_name, ""+artistname);

            status.contentView = views;
            status.bigContentView = views;
            startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, status);
        }
        else if (intent.getAction().equals(Constants.ACTION.PLAY_ACTION)) {

            if (!mp.isPlaying()) {
                // Stopping
                mp.start();
            //    bigViews.setImageViewResource(R.id.status_bar_play, R.drawable.pause);
                views.setImageViewResource(R.id.status_bar_play, R.drawable.pause);
                ((MainActivity) context).synchronizemain("start");

                status.contentView = views;
                status.bigContentView = views;
                startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, status);
            } else {
                // Playing
                mp.pause();
           //   bigViews.setImageViewResource(R.id.status_bar_play, R.drawable.play);
                views.setImageViewResource(R.id.status_bar_play, R.drawable.play);
                ((MainActivity) context).synchronizemain("pause");

                status.contentView = views;
                status.bigContentView = views;
                startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, status);
            }

          //  Toast.makeText(this, "Clicked Play", Toast.LENGTH_SHORT).show();
            Log.i(LOG_TAG, "Clicked Play");

        } else if (intent.getAction().equals(Constants.ACTION.NEXT_ACTION)) {
         //   Toast.makeText(this, "Clicked Next", Toast.LENGTH_SHORT).show();
            Log.i(LOG_TAG, "Clicked Next");

            ((MainActivity) context).playnext();
        } else if (intent.getAction().equals(Constants.ACTION.STOPFOREGROUND_ACTION)) {
            // Stopping

            try
            {
                mp.stop();
                views.setImageViewResource(R.id.status_bar_play, R.drawable.play);
                //  bigViews.setImageViewResource(R.id.status_bar_play, R.drawable.play);
                ((MainActivity) context).synchronizemain("Stop");

                Log.i(LOG_TAG, "Received Stop Foreground Intent");
                //  Toast.makeText(this, "Service Stoped", Toast.LENGTH_SHORT).show();
                stopForeground(true);
                stopSelf();
            }
            catch (Exception e)
            {
                mp.stop();
                views.setImageViewResource(R.id.status_bar_play, R.drawable.play);
                //  bigViews.setImageViewResource(R.id.status_bar_play, R.drawable.play);
                ((MainActivity) context).synchronizemain("Stop");

                Log.i(LOG_TAG, "Received Stop Foreground Intent");
                //  Toast.makeText(this, "Service Stoped", Toast.LENGTH_SHORT).show();
                stopForeground(true);
                stopSelf();
            }
        }
        else if (intent.getAction().equals(Constants.ACTION.STOPFORLOCAL)) {
            // Stopping
            mp.stop();
            views.setImageViewResource(R.id.status_bar_play, R.drawable.play);
            //  bigViews.setImageViewResource(R.id.status_bar_play, R.drawable.play);
            ((MainActivity) context).synchronizemain("DONTHIDE");

            Log.i(LOG_TAG, "Received Stop Foreground Intent");
         //   Toast.makeText(this, "Service Stoped", Toast.LENGTH_SHORT).show();
            stopForeground(true);
            stopSelf();

            ((MainActivity) context).play_local_music(userr, localmusicpath);

        }
        else if (intent.getAction().equals(Constants.ACTION.sync))
        {
            if (mp.isPlaying()) {
                // Stopping
                mp.start();
             //   bigViews.setImageViewResource(R.id.status_bar_play, R.drawable.pause);
                views.setImageViewResource(R.id.status_bar_play, R.drawable.pause);

                status.contentView = views;
                status.bigContentView = views;
                startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, status);
            } else {
                // Playing
                mp.pause();
             //   bigViews.setImageViewResource(R.id.status_bar_play, R.drawable.play);
                views.setImageViewResource(R.id.status_bar_play, R.drawable.play);

                status.contentView = views;
                status.bigContentView = views;
                startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, status);
            }
        }

        else if (intent.getAction().equals(Constants.ACTION.PausePlayer))
        {
                mp.pause();
                //   bigViews.setImageViewResource(R.id.status_bar_play, R.drawable.play);
                views.setImageViewResource(R.id.status_bar_play, R.drawable.play);

                status.contentView = views;
                status.bigContentView = views;
                startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, status);
        }


        else if (intent.getAction().equals(Constants.ACTION.StartPlayer))
        {
            mp.start();
            //   bigViews.setImageViewResource(R.id.status_bar_play, R.drawable.pause);
            views.setImageViewResource(R.id.status_bar_play, R.drawable.pause);

            status.contentView = views;
            status.bigContentView = views;
            startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, status);
        }

        return START_STICKY;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void showNotification(String change) {
        // Using RemoteViews to bind custom layouts into Notification
        views = new RemoteViews(getPackageName(), R.layout.status_bar);
      // bigViews = new RemoteViews(getPackageName(), R.layout.status_bar_expanded);

          // showing default album image
         //  views.setViewVisibility(R.id.status_bar_icon, View.VISIBLE);
        //   views.setViewVisibility(R.id.status_bar_album_art, View.GONE);
       // views.setImageViewResource(R.id.status_bar_album_art, R.drawable.playample);
      //  bigViews.setImageViewResource(R.id.status_bar_album_art, R.drawable.playample);

   //       notificationIntent = new Intent(this, MainActivity.class);
  //        notificationIntent.setAction(Constants.ACTION.MAIN_ACTION);
 //        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);


        Intent previousIntent = new Intent(this, NotificationService.class);
        previousIntent.setAction(Constants.ACTION.PREV_ACTION);
        PendingIntent ppreviousIntent = PendingIntent.getService(this, 0, previousIntent, 0);

        Intent playIntent = new Intent(this, NotificationService.class);
        playIntent.setAction(Constants.ACTION.PLAY_ACTION);
        PendingIntent pplayIntent = PendingIntent.getService(this, 0, playIntent, 0);

        Intent nextIntent = new Intent(this, NotificationService.class);
        nextIntent.setAction(Constants.ACTION.NEXT_ACTION);
        PendingIntent pnextIntent = PendingIntent.getService(this, 0, nextIntent, 0);

        Intent closeIntent = new Intent(this, NotificationService.class);
        closeIntent.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
        PendingIntent pcloseIntent = PendingIntent.getService(this, 0, closeIntent, 0);

        if (change.equals("pause")) {
            views.setImageViewResource(R.id.status_bar_play, R.drawable.pause);
     //   bigViews.setImageViewResource(R.id.status_bar_play, R.drawable.pause);
        } else {
            views.setImageViewResource(R.id.status_bar_play, R.drawable.play);
     //   bigViews.setImageViewResource(R.id.status_bar_play, R.drawable.play);
        }

        views.setOnClickPendingIntent(R.id.status_bar_play, pplayIntent);
     //   bigViews.setOnClickPendingIntent(R.id.status_bar_play, pplayIntent);

        views.setOnClickPendingIntent(R.id.status_bar_next, pnextIntent);
     //   bigViews.setOnClickPendingIntent(R.id.status_bar_next, pnextIntent);

        views.setOnClickPendingIntent(R.id.status_bar_prev, ppreviousIntent);
     //   bigViews.setOnClickPendingIntent(R.id.status_bar_prev, ppreviousIntent);

        views.setOnClickPendingIntent(R.id.status_bar_collapse, pcloseIntent);
      //  bigViews.setOnClickPendingIntent(R.id.status_bar_collapse, pcloseIntent);

        views.setTextViewText(R.id.status_bar_track_name, "Song Title");
      //  bigViews.setTextViewText(R.id.status_bar_track_name, "Song Title");

        views.setTextViewText(R.id.status_bar_artist_name, "Artist Name");
     //  bigViews.setTextViewText(R.id.status_bar_artist_name, "Artist Name");

     //  bigViews.setTextViewText(R.id.status_bar_album_name, "Album Name");

        manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String CHANNEL_ID = "alex_channel";
            channel = new NotificationChannel(CHANNEL_ID, "AlexChannel", NotificationManager.IMPORTANCE_LOW);
            channel.setDescription("Alex channel description");
            channel.setSound(null, null);
            manager.createNotificationChannel(channel);
            status = new Notification.Builder(this, CHANNEL_ID).setSound(null).setDefaults(Notification.DEFAULT_ALL).build();
        }
        else {
            status = new Notification.Builder(this).setSound(null).setDefaults(0).build();
        }

        status.contentView = views;
        status.bigContentView = views;
        status.flags = Notification.FLAG_ONGOING_EVENT;
        status.icon = R.mipmap.ic_launcher;
        status.contentIntent = pendingIntent;
        status.defaults = 0;
        startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, status);
    }

    public static void recmedia(MediaPlayer mediaPlayer, MainActivity mainActivity) {
        mp = null;
        mp = mediaPlayer;
        context = mainActivity;
    }

    public static void reclocalobject(User user, String path){
        userr =user;
        localmusicpath=path;
    }
}