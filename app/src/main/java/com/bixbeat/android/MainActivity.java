package com.bixbeat.android;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.database.Cursor;
import android.graphics.Color;;
import android.graphics.drawable.ColorDrawable;
import android.icu.text.SimpleDateFormat;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import com.bixbeat.android.Model.DownloadSong;
import com.bixbeat.android.Model.Facebookdata;
import com.bixbeat.android.Model.Loggeddata;
import com.bixbeat.android.Model.SongData;
import com.bixbeat.android.Model.Song_;
import com.bixbeat.android.roomdatabase.AppDatabase;
import com.bixbeat.android.roomdatabase.User;
import com.bixbeat.android.utils.Constants;
import com.bixbeat.android.utils.CurrentAdapter;
import com.bixbeat.android.utils.DirectoryHelper;
import com.bixbeat.android.utils.LibraryAdapter;
import com.bixbeat.android.utils.NetworkConnectionChecker;
import com.bixbeat.android.utils.NotificationService;
import com.chibde.visualizer.CircleBarVisualizer;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, NetworkConnectionChecker.OnConnectivityChangedListener, GoogleApiClient.OnConnectionFailedListener {

    WebView webView;
    LinearLayout ll_music_mini;
    ImageView img_pp_mini, img_big;
    MediaPlayer mediaPlayer;
    SeekBar positionBar;
    TextView txt_song_name_big, txt_album_name_big, end_time, start_time, txt_mini_subtitle, txt_mini_title, txt_mess_name, txt_home, txt_home_1, txt_menu, txt_menu_1, txt_search, txt_search_1, txt_channel, txt_channel_1, txt_library, txt_library_1, txt_profile, txt_profile_1;
    int finalTime, startTime;
    String decision = "";
    int position = 0;
    SongData songData;
    Button but_min_view, but_max_view;
    CircleImageView album_small, img_music_mini;
    CardView card_pp;
    ImageView img_pp, but_next_mini, but_prev_mini, cross_back_lib, img_user, img_home, img_search, img_channel, img_lib, img_menu, img_user_1, img_home_1, img_search_1, img_channel_1, img_lib_1, img_menu_1;
    CircleBarVisualizer circleBarVisualizer;
    LinearLayout ll_web_parent, ll_min, ll_cross_mini, ll_mini_player_pause, llWeblayout, ll_previous_song, ll_next_song, ll_loader, ll_player, ll_mini_play,
            ll_mini_progress, ll_download, ll_music_lib, ll_music_library, ll_empty_music_state, ll_user, ll_home, ll_search, ll_channel, ll_my_lib, ll_menu, ll_user_1,
            ll_home_1, ll_search_1, ll_channel_1, ll_my_lib_1, ll_menu_1, ll_dj_mixes, ll_term, ll_about, ll_record_label;

    private BottomSheetBehavior bottomSheetBehavior;
    LinearLayout ll_bottomSheetHeading;
    ProgressBar progress_bar, mini_player_progress;
    public WebAppInterface webAppInterface;
    Intent serviceIntent;
    DownloadManager downloadManager;
    CircleBarVisualizer temp_bar_visual;
    private Boolean exit = false;
    User user;
    private long downloadID;
    String temp_song_name = "";
    String temp_artist_name = "";
    String temp_album_path = "";
    String temp_songpath = "";
    private static String TAG = "RoomDB";
    private AppDatabase mDb;
    File file;
    Gson gson;
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    LibraryAdapter libraryAdapter;
    List<User> users;
    String templocal_path = "";
    File myDirectory;
    Boolean local = false;
    ArrayList<User> localplaylist;
    int Localplayposition = 0;
    private NetworkConnectionChecker networkConnectionChecker;
    Handler handler;
    CardView card_prem_alert;
    Boolean localplay = false;
    int currentPosition = 0;
    Boolean endreach = false;
    int bottomsheetstate = 0;
    Dialog remdiag, menudiag;
    boolean premiumuser = false;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 007;
    private int mFacerequestcode = 64206;
    private CallbackManager callbackManager;
    PackageInfo info;
    String ns = Context.NOTIFICATION_SERVICE;
    NotificationManager notificationManager;
    Dialog dialog;
    boolean SIGNINState;
    private String mCM;
    private ValueCallback<Uri> mUM;
    private ValueCallback<Uri[]> mUMA;
    private final static int FCR = 1;
    private final static int FILECHOOSER_RESULTCODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        user = new User();
        DirectoryHelper.createDirectory(this);
        localplaylist = new ArrayList<>();
        card_prem_alert = (CardView) findViewById(R.id.card_prem_alert);
        mini_player_progress = (ProgressBar) findViewById(R.id.mini_player_progress);
        ll_music_library = (LinearLayout) findViewById(R.id.ll_music_library);
        ll_empty_music_state = (LinearLayout) findViewById(R.id.ll_empty_music_state);
        ll_music_lib = (LinearLayout) findViewById(R.id.ll_music_lib);
        ll_download = (LinearLayout) findViewById(R.id.ll_download);
        ll_mini_progress = (LinearLayout) findViewById(R.id.ll_mini_progress);
        ll_mini_play = (LinearLayout) findViewById(R.id.ll_mini_play);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        but_prev_mini = (ImageView) findViewById(R.id.but_prev_mini);
        but_next_mini = (ImageView) findViewById(R.id.but_next_mini);
        cross_back_lib = (ImageView) findViewById(R.id.cross_back_lib);
        img_big = (ImageView) findViewById(R.id.img_big);
        txt_mess_name = (TextView) findViewById(R.id.txt_mess_name);
        album_small = (CircleImageView) findViewById(R.id.album_small);
        ll_player = (LinearLayout) findViewById(R.id.ll_player);
        ll_loader = (LinearLayout) findViewById(R.id.ll_loader);
        txt_song_name_big = (TextView) findViewById(R.id.txt_song_name_big);
        txt_album_name_big = (TextView) findViewById(R.id.txt_album_name_big);
        txt_mini_title = (TextView) findViewById(R.id.txt_mini_title);
        txt_mini_subtitle = (TextView) findViewById(R.id.txt_mini_subtitle);
        webView = (WebView) findViewById(R.id.bix_webview);
        img_music_mini = (CircleImageView) findViewById(R.id.img_music_mini);
        img_pp_mini = (ImageView) findViewById(R.id.img_pp_mini);
        img_search = (ImageView) findViewById(R.id.img_search);
        img_channel = (ImageView) findViewById(R.id.img_channel);
        img_lib = (ImageView) findViewById(R.id.img_lib);
        img_menu = (ImageView) findViewById(R.id.img_menu);
        img_user = (ImageView) findViewById(R.id.img_user);
        img_home = (ImageView) findViewById(R.id.img_home);
        img_search_1 = (ImageView) findViewById(R.id.img_search_1);
        img_channel_1 = (ImageView) findViewById(R.id.img_channel_1);
        img_lib_1 = (ImageView) findViewById(R.id.img_lib_1);
        img_menu_1 = (ImageView) findViewById(R.id.img_menu_1);
        img_user_1 = (ImageView) findViewById(R.id.img_user_1);
        img_home_1 = (ImageView) findViewById(R.id.img_home_1);
        ll_music_mini = (LinearLayout) findViewById(R.id.ll_music_mini);
        ll_cross_mini = (LinearLayout) findViewById(R.id.ll_cross_mini);
        ll_bottomSheetHeading = (LinearLayout) findViewById(R.id.ll_bottomSheetHeading);
        ll_mini_player_pause = (LinearLayout) findViewById(R.id.ll_mini_player_pause);
        llWeblayout = (LinearLayout) findViewById(R.id.ll_web);
        ll_web_parent = findViewById(R.id.ll_web_parent);
        ll_next_song = (LinearLayout) findViewById(R.id.ll_next_song);
        ll_previous_song = (LinearLayout) findViewById(R.id.ll_previous_song);
        but_min_view = (Button) findViewById(R.id.but_min_view);
        but_max_view = (Button) findViewById(R.id.but_max_view);
        start_time = (TextView) findViewById(R.id.start_time);
        end_time = (TextView) findViewById(R.id.end_time);
        circleBarVisualizer = findViewById(R.id.visualizer);
        ll_search = (LinearLayout) findViewById(R.id.ll_search);
        ll_channel = (LinearLayout) findViewById(R.id.ll_channel);
        ll_user = (LinearLayout) findViewById(R.id.ll_user);
        ll_my_lib = (LinearLayout) findViewById(R.id.ll_my_lib);
        ll_menu = (LinearLayout) findViewById(R.id.ll_menu);
        ll_home = (LinearLayout) findViewById(R.id.ll_home);
        ll_search_1 = (LinearLayout) findViewById(R.id.ll_search_1);
        ll_channel_1 = (LinearLayout) findViewById(R.id.ll_channel_1);
        ll_user_1 = (LinearLayout) findViewById(R.id.ll_user_1);
        ll_my_lib_1 = (LinearLayout) findViewById(R.id.ll_my_lib_1);
        ll_menu_1 = (LinearLayout) findViewById(R.id.ll_menu_1);
        ll_home_1 = (LinearLayout) findViewById(R.id.ll_home_1);

        txt_home = (TextView) findViewById(R.id.txt_home);
        txt_home_1 = (TextView) findViewById(R.id.txt_home_1);
        txt_menu_1 = (TextView) findViewById(R.id.txt_menu_1);
        txt_menu = (TextView) findViewById(R.id.txt_menu);
        txt_search = (TextView) findViewById(R.id.txt_search);
        txt_search_1 = (TextView) findViewById(R.id.txt_search_1);
        txt_channel = (TextView) findViewById(R.id.txt_channel);
        txt_channel_1 = (TextView) findViewById(R.id.txt_channel_1);
        txt_library = (TextView) findViewById(R.id.txt_library);
        txt_library_1 = (TextView) findViewById(R.id.txt_library_1);
        txt_profile = (TextView) findViewById(R.id.txt_profile);
        txt_profile_1 = (TextView) findViewById(R.id.txt_profile_1);


        webAppInterface = new WebAppInterface(MainActivity.this);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.setBackgroundColor(Color.argb(1, 0, 0, 0));
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setAllowContentAccess(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setAllowContentAccess(true);
        webView.addJavascriptInterface(webAppInterface, "Android");
        webView.setWebViewClient(new MyBrowser());

        temp_bar_visual = circleBarVisualizer;
        recyclerView = findViewById(R.id.my_recycle_main);
        webView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                // signIn();
                //facebook_login();
                return false;
            }
        });

        //Webview upload file
        webView.setWebChromeClient(new WebChromeClient() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, WebChromeClient.FileChooserParams fileChooserParams) {
                if (mUMA != null) {
                    mUMA.onReceiveValue(null);
                }
                mUMA = filePathCallback;
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(MainActivity.this.getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                        takePictureIntent.putExtra("PhotoPath", mCM);
                    } catch (IOException ex) {
                        Log.e("Webview", "Image file creation failed", ex);
                    }
                    if (photoFile != null) {
                        mCM = "file:" + photoFile.getAbsolutePath();
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                    } else {
                        takePictureIntent = null;
                    }
                }

                Intent contentSelectionIntent = new Intent(Intent.ACTION_GET_CONTENT);
                contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE);
                contentSelectionIntent.setType("*/*");
                Intent[] intentArray;
                if (takePictureIntent != null) {
                    intentArray = new Intent[]{takePictureIntent};
                } else {
                    intentArray = new Intent[0];
                }

                Intent chooserIntent = new Intent(Intent.ACTION_CHOOSER);
                chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent);
                chooserIntent.putExtra(Intent.EXTRA_TITLE, "Image Chooser");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
                startActivityForResult(chooserIntent, FCR);
                return true;
            }
        });


        notificationManager = (NotificationManager) getApplicationContext().getSystemService(ns);

//        try {
//            info = getPackageManager().getPackageInfo("com.bixbeat", PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md;
//                md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                String something = new String(Base64.encode(md.digest(), 0));
//                //String something = new String(Base64.encodeBytes(md.digest()));
//                Log.e("hash key", something);
//            }
//        } catch (PackageManager.NameNotFoundException e1) {
//            Log.e("name not found", e1.toString());
//        } catch (NoSuchAlgorithmException e) {
//            Log.e("no such an algorithm", e.toString());
//        } catch (Exception e) {
//            Log.e("exception", e.toString());
//        }

        String serverClientId = getString(R.string.server_client_id);
        callbackManager = CallbackManager.Factory.create();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestServerAuthCode(serverClientId)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, MainActivity.this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
//Broadcast for download
        registerReceiver(onDownloadComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        registerReceiver(downloadReceiver, new IntentFilter(DownloadManager.ACTION_NOTIFICATION_CLICKED));
        registerReceiver(downloadclick, new IntentFilter(DownloadManager.ACTION_VIEW_DOWNLOADS));

        IntentFilter filter = new IntentFilter();
        filter.addAction(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        filter.addAction(DownloadManager.ACTION_NOTIFICATION_CLICKED);
        filter.addAction(DownloadManager.ACTION_VIEW_DOWNLOADS);


        BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //do something based on the intent's action
                if (intent.getAction().equals(DownloadManager.ACTION_DOWNLOAD_COMPLETE)) {
                  //  Toast.makeText(MainActivity.this, "letsee", Toast.LENGTH_SHORT).show();
                } else if (intent.getAction().equals(DownloadManager.ACTION_NOTIFICATION_CLICKED)) {
                  //  Toast.makeText(MainActivity.this, "letsee", Toast.LENGTH_SHORT).show();
                } else if (intent.getAction().equals(DownloadManager.ACTION_VIEW_DOWNLOADS)) {
                  //  Toast.makeText(MainActivity.this, "letsee", Toast.LENGTH_SHORT).show();
                }
            }
        };

        registerReceiver(receiver, filter);

        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);

        but_max_view.setOnClickListener(this);
        but_min_view.setOnClickListener(this);
        ll_music_mini.setOnClickListener(this);
        img_music_mini.setOnClickListener(this);
        ll_previous_song.setOnClickListener(this);
        ll_next_song.setOnClickListener(this);
        but_prev_mini.setOnClickListener(this);
        but_next_mini.setOnClickListener(this);
        ll_download.setOnClickListener(this);
        ll_music_lib.setOnClickListener(this);
        card_prem_alert.setOnClickListener(this);
        ll_search.setOnClickListener(this);
        ll_channel.setOnClickListener(this);
        ll_user.setOnClickListener(this);
        ll_my_lib.setOnClickListener(this);
        ll_menu.setOnClickListener(this);
        ll_home.setOnClickListener(this);
        ll_search_1.setOnClickListener(this);
        ll_channel_1.setOnClickListener(this);
        ll_user_1.setOnClickListener(this);
        ll_my_lib_1.setOnClickListener(this);
        ll_menu_1.setOnClickListener(this);
        ll_home_1.setOnClickListener(this);
        //dialog intialization
        remdiag = new Dialog(MainActivity.this);
        remdiag.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        remdiag.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        remdiag.setContentView(R.layout.dialog_prem);
        remdiag.setCancelable(true);

        dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.internet_layout);
        dialog.setCancelable(false);

        Button but_sub = (Button) remdiag.findViewById(R.id.but_sub);

        but_sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                paymentlaunch();
                remdiag.cancel();
            }
        });

        songData = new SongData();

        networkConnectionChecker = new NetworkConnectionChecker(getApplication());
        networkConnectionChecker.registerListener(this);

        initViews();
        initListeners();

        positionBar = (SeekBar) findViewById(R.id.songProgressBar);
        mediaPlayer = new MediaPlayer();

        card_pp = (CardView) findViewById(R.id.card_pp);
        img_pp = (ImageView) findViewById(R.id.img_pp);
        ll_min = (LinearLayout) findViewById(R.id.ll_min);

        ll_min.setOnClickListener(this);
        card_pp.setOnClickListener(this);
        ll_cross_mini.setOnClickListener(this);
        ll_mini_player_pause.setOnClickListener(this);
        cross_back_lib.setOnClickListener(this);

        attachseekbarlistener();
//Timer for Seekbar
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (mediaPlayer != null) {
                    try {
                        Message msg = new Message();
                        msg.what = mediaPlayer.getCurrentPosition();
                        handlerrr.sendMessage(msg);
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                    }
                }
            }
        }).start();


//        try {
//            MainActivity.this.runOnUiThread(new Runnable() {
//                @TargetApi(Build.VERSION_CODES.KITKAT)
//                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
//                public void run() {
//                    webView.evaluateJavascript("javascript:window.Android.onUserLoggedIn(state, userLocal)", new ValueCallback<String>() {
//                        @Override
//                        public void onReceiveValue(String s) {
//                            // Do what you want with the return value
//                            System.out.println(">>>>."+s);
//                        }
//                    });
//                }
//            });
//        }
//        catch (Exception e) {
//
//        }

        //checkstate();
    }

    public void openFileChooser(ValueCallback<Uri> uploadMsg) {
        this.openFileChooser(uploadMsg, "*/*");
    }

    public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {
        this.openFileChooser(uploadMsg, acceptType, null);
    }

    public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.addCategory(Intent.CATEGORY_OPENABLE);
        i.setType("*/*");
        MainActivity.this.startActivityForResult(Intent.createChooser(i, "File Browser"), FILECHOOSER_RESULTCODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (Build.VERSION.SDK_INT >= 21) {
            Uri[] results = null;
            //Check if response is positive
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == FCR) {
                    if (null == mUMA) {
                        return;
                    }
                    if (intent == null) {
                        //Capture Photo if no image available
                        if (mCM != null) {
                            results = new Uri[]{Uri.parse(mCM)};
                        }
                    } else {
                        String dataString = intent.getDataString();
                        if (dataString != null) {
                            results = new Uri[]{Uri.parse(dataString)};
                        }
                    }
                }
            }
            mUMA.onReceiveValue(results);
            mUMA = null;
        } else {
            if (requestCode == FCR) {
                if (null == mUM) return;
                Uri result = intent == null || resultCode != RESULT_OK ? null : intent.getData();
                mUM.onReceiveValue(result);
                mUM = null;
            }
        }
    }

    // Create an image file
    @RequiresApi(api = Build.VERSION_CODES.N)
    private File createImageFile() throws IOException {
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "img_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(imageFileName, ".jpg", storageDir);
    }



    public String createTimeLabel(int time) {
        String timeLabel = "";
        int min = time / 1000 / 60;
        int sec = time / 1000 % 60;

        timeLabel = min + ":";
        if (sec < 10) timeLabel += "0";
        timeLabel += sec;

        return timeLabel;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.ll_music_mini:

                launchplayer();

                break;

            case R.id.ll_channel:


                if (!isOnline(MainActivity.this)) {
                    internetdialog();
                    //no_int_song=true;
                } else {
                    cross_back_lib.performClick();
                    //loadurll("https://bixbeat.com/channels/?platform=android");
                    loadfunurll("Channels");
                    img_search.setImageResource(R.drawable.search_unselected);
                    img_channel.setImageResource(R.drawable.channel_selected);
                    img_lib.setImageResource(R.drawable.lib_unselected);
                    img_menu.setImageResource(R.drawable.menu_unselected);
                    img_user.setImageResource(R.drawable.user_unselected);
                    img_home.setImageResource(R.drawable.home_unselect);

                    img_search_1.setImageResource(R.drawable.search_unselected);
                    img_channel_1.setImageResource(R.drawable.channel_selected);
                    img_lib_1.setImageResource(R.drawable.lib_unselected);
                    img_menu_1.setImageResource(R.drawable.menu_unselected);
                    img_user_1.setImageResource(R.drawable.user_unselected);
                    img_home_1.setImageResource(R.drawable.home_unselect);

                    txt_home.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_menu.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_search.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_channel.setTextColor(getResources().getColor(R.color.icon_red));
                    txt_library.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_profile.setTextColor(getResources().getColor(R.color.icon_grey));

                    txt_home_1.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_menu_1.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_search_1.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_channel_1.setTextColor(getResources().getColor(R.color.icon_red));
                    txt_library_1.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_profile_1.setTextColor(getResources().getColor(R.color.icon_grey));
                }

                break;

            case R.id.ll_user:


                if (!isOnline(MainActivity.this)) {
                    internetdialog();
                    //no_int_song=true;
                } else {

                    cross_back_lib.performClick();
                   // loadurll("https://bixbeat.com/profile/?platform=android");
                    loadfunurll("Sign Up");
                    img_search.setImageResource(R.drawable.search_unselected);
                    img_channel.setImageResource(R.drawable.channel_unselected);
                    img_lib.setImageResource(R.drawable.lib_unselected);
                    img_menu.setImageResource(R.drawable.menu_unselected);
                    img_user.setImageResource(R.drawable.user_selected);
                    img_home.setImageResource(R.drawable.home_unselect);


                    img_search_1.setImageResource(R.drawable.search_unselected);
                    img_channel_1.setImageResource(R.drawable.channel_unselected);
                    img_lib_1.setImageResource(R.drawable.lib_unselected);
                    img_menu_1.setImageResource(R.drawable.menu_unselected);
                    img_user_1.setImageResource(R.drawable.user_selected);
                    img_home_1.setImageResource(R.drawable.home_unselect);

                    txt_home.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_menu.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_search.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_channel.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_library.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_profile.setTextColor(getResources().getColor(R.color.icon_red));

                    txt_home_1.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_menu_1.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_search_1.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_channel_1.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_library_1.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_profile_1.setTextColor(getResources().getColor(R.color.icon_red));
                }
                break;

            case R.id.ll_my_lib:


                if (bottomsheetstate == BottomSheetBehavior.STATE_COLLAPSED) {

                    ll_download.performClick();
                } else {
                    txt_mess_name.setText("Music Library");
                    ll_music_library.setVisibility(View.VISIBLE);
                    llWeblayout.setVisibility(View.GONE);

                    handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            AsyncTask.execute(new Runnable() {
                                @Override
                                public void run() {
                                    mDb = AppDatabase.getInMemoryDatabase(getApplicationContext());
                                    listUsers();
                                }
                            });
                        }
                    }, 5);
                }


                img_search.setImageResource(R.drawable.search_unselected);
                img_channel.setImageResource(R.drawable.channel_unselected);
                img_lib.setImageResource(R.drawable.lib_selected);
                img_menu.setImageResource(R.drawable.menu_unselected);
                img_user.setImageResource(R.drawable.user_unselected);
                img_home.setImageResource(R.drawable.home_unselect);

                img_search_1.setImageResource(R.drawable.search_unselected);
                img_channel_1.setImageResource(R.drawable.channel_unselected);
                img_lib_1.setImageResource(R.drawable.lib_selected);
                img_menu_1.setImageResource(R.drawable.menu_unselected);
                img_user_1.setImageResource(R.drawable.user_unselected);
                img_home_1.setImageResource(R.drawable.home_unselect);

                txt_home.setTextColor(getResources().getColor(R.color.icon_grey));
                txt_menu.setTextColor(getResources().getColor(R.color.icon_grey));
                txt_search.setTextColor(getResources().getColor(R.color.icon_grey));
                txt_channel.setTextColor(getResources().getColor(R.color.icon_grey));
                txt_library.setTextColor(getResources().getColor(R.color.icon_red));
                txt_profile.setTextColor(getResources().getColor(R.color.icon_grey));

                txt_home_1.setTextColor(getResources().getColor(R.color.icon_grey));
                txt_menu_1.setTextColor(getResources().getColor(R.color.icon_grey));
                txt_search_1.setTextColor(getResources().getColor(R.color.icon_grey));
                txt_channel_1.setTextColor(getResources().getColor(R.color.icon_grey));
                txt_library_1.setTextColor(getResources().getColor(R.color.icon_red));
                txt_profile_1.setTextColor(getResources().getColor(R.color.icon_grey));

                break;

            case R.id.ll_menu:

                if (!isOnline(MainActivity.this)) {
                    internetdialog();
                    //no_int_song=true;
                } else {
                    cross_back_lib.performClick();
                    showmenu();
                    img_search.setImageResource(R.drawable.search_unselected);
                    img_channel.setImageResource(R.drawable.channel_unselected);
                    img_lib.setImageResource(R.drawable.lib_unselected);
                    img_menu.setImageResource(R.drawable.menu_selected);
                    img_user.setImageResource(R.drawable.user_unselected);
                    img_home.setImageResource(R.drawable.home_unselect);

                    img_search_1.setImageResource(R.drawable.search_unselected);
                    img_channel_1.setImageResource(R.drawable.channel_unselected);
                    img_lib_1.setImageResource(R.drawable.lib_unselected);
                    img_menu_1.setImageResource(R.drawable.menu_selected);
                    img_user_1.setImageResource(R.drawable.user_unselected);
                    img_home_1.setImageResource(R.drawable.home_unselect);

                    txt_home.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_menu.setTextColor(getResources().getColor(R.color.icon_red));
                    txt_search.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_channel.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_library.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_profile.setTextColor(getResources().getColor(R.color.icon_grey));

                    txt_home_1.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_menu_1.setTextColor(getResources().getColor(R.color.icon_red));
                    txt_search_1.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_channel_1.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_library_1.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_profile_1.setTextColor(getResources().getColor(R.color.icon_grey));
                }


                break;

            case R.id.ll_home:


                if (!isOnline(MainActivity.this)) {
                    internetdialog();
                    //no_int_song=true;
                } else {
                    cross_back_lib.performClick();
                   // loadurll("https://bixbeat.com/home/?platform=android");
                    loadfunurll("Home");
                    img_search.setImageResource(R.drawable.search_unselected);
                    img_channel.setImageResource(R.drawable.channel_unselected);
                    img_lib.setImageResource(R.drawable.lib_unselected);
                    img_menu.setImageResource(R.drawable.menu_unselected);
                    img_user.setImageResource(R.drawable.user_unselected);
                    img_home.setImageResource(R.drawable.home_select);

                    img_search_1.setImageResource(R.drawable.search_unselected);
                    img_channel_1.setImageResource(R.drawable.channel_unselected);
                    img_lib_1.setImageResource(R.drawable.lib_unselected);
                    img_menu_1.setImageResource(R.drawable.menu_unselected);
                    img_user_1.setImageResource(R.drawable.user_unselected);
                    img_home_1.setImageResource(R.drawable.home_select);

                    txt_home.setTextColor(getResources().getColor(R.color.icon_red));
                    txt_menu.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_search.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_channel.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_library.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_profile.setTextColor(getResources().getColor(R.color.icon_grey));

                    txt_home_1.setTextColor(getResources().getColor(R.color.icon_red));
                    txt_menu_1.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_search_1.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_channel_1.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_library_1.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_profile_1.setTextColor(getResources().getColor(R.color.icon_grey));
                }
                break;

            case R.id.ll_search:


                if (!isOnline(MainActivity.this)) {
                    internetdialog();
                    //no_int_song=true;
                } else {
                   // loadurll("https://bixbeat.com/search/?platform=android");
                    loadfunurll("Search");
                    cross_back_lib.performClick();
                    img_search.setImageResource(R.drawable.search_selected);
                    img_channel.setImageResource(R.drawable.channel_unselected);
                    img_lib.setImageResource(R.drawable.lib_unselected);
                    img_menu.setImageResource(R.drawable.menu_unselected);
                    img_user.setImageResource(R.drawable.user_unselected);
                    img_home.setImageResource(R.drawable.home_unselect);

                    img_search_1.setImageResource(R.drawable.search_selected);
                    img_channel_1.setImageResource(R.drawable.channel_unselected);
                    img_lib_1.setImageResource(R.drawable.lib_unselected);
                    img_menu_1.setImageResource(R.drawable.menu_unselected);
                    img_user_1.setImageResource(R.drawable.user_unselected);
                    img_home_1.setImageResource(R.drawable.home_unselect);

                    txt_home.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_menu.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_search.setTextColor(getResources().getColor(R.color.icon_red));
                    txt_channel.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_library.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_profile.setTextColor(getResources().getColor(R.color.icon_grey));

                    txt_home_1.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_menu_1.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_search_1.setTextColor(getResources().getColor(R.color.icon_red));
                    txt_channel_1.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_library_1.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_profile_1.setTextColor(getResources().getColor(R.color.icon_grey));
                }

                break;

            case R.id.ll_channel_1:

                ll_channel.performClick();

                break;

            case R.id.ll_user_1:

                ll_user.performClick();

                break;

            case R.id.ll_my_lib_1:

                ll_my_lib.performClick();

                break;

            case R.id.ll_menu_1:

                ll_menu.performClick();

                break;

            case R.id.ll_home_1:

                ll_home.performClick();

                break;

            case R.id.ll_search_1:

                ll_search.performClick();

                break;

            case R.id.card_prem_alert:

                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                paymentlaunch();

                break;


            case R.id.cross_back_lib:

                if (!isOnline(MainActivity.this)) {
                    internetdialog();
                    //no_int_song=true;
                } else {

                    MainActivity.this.runOnUiThread(new Runnable() {
                        public void run() {

                            ll_music_library.setVisibility(View.GONE);
                            llWeblayout.setVisibility(View.VISIBLE);
                        }
                    });
                }

                break;

            case R.id.ll_music_lib:

                if (!isOnline(MainActivity.this)) {
                    internetdialog();
                    //no_int_song=true;
                } else {
                    localplay = false;
                    txt_mess_name.setText("Current Playlist");
                    ll_empty_music_state.setVisibility(View.INVISIBLE);
                    ll_music_library.setVisibility(View.VISIBLE);
                    llWeblayout.setVisibility(View.GONE);
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

                    MainActivity.this.runOnUiThread(new Runnable() {
                        public void run() {

                            handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    recyclerView = findViewById(R.id.my_recycle_main);
                                    layoutManager = new LinearLayoutManager(MainActivity.this, RecyclerView.VERTICAL, false);
                                    recyclerView.setLayoutManager(layoutManager);
                                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                                    CurrentAdapter currentAdapter = new CurrentAdapter(MainActivity.this, songData.getSongs());
                                    recyclerView.setAdapter(currentAdapter);
                                }
                            }, 350);
                        }
                    });

                    img_search.setImageResource(R.drawable.search_unselected);
                    img_channel.setImageResource(R.drawable.channel_unselected);
                    img_lib.setImageResource(R.drawable.lib_selected);
                    img_menu.setImageResource(R.drawable.menu_unselected);
                    img_user.setImageResource(R.drawable.user_unselected);
                    img_home.setImageResource(R.drawable.home_unselect);

                    img_search_1.setImageResource(R.drawable.search_unselected);
                    img_channel_1.setImageResource(R.drawable.channel_unselected);
                    img_lib_1.setImageResource(R.drawable.lib_selected);
                    img_menu_1.setImageResource(R.drawable.menu_unselected);
                    img_user_1.setImageResource(R.drawable.user_unselected);
                    img_home_1.setImageResource(R.drawable.home_unselect);

                    txt_home.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_menu.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_search.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_channel.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_library.setTextColor(getResources().getColor(R.color.icon_red));
                    txt_profile.setTextColor(getResources().getColor(R.color.icon_grey));

                    txt_home_1.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_menu_1.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_search_1.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_channel_1.setTextColor(getResources().getColor(R.color.icon_grey));
                    txt_library_1.setTextColor(getResources().getColor(R.color.icon_red));
                    txt_profile_1.setTextColor(getResources().getColor(R.color.icon_grey));

                }


                break;

            case R.id.but_next_mini:

                ll_next_song.performClick();

                break;

            case R.id.but_prev_mini:

                ll_previous_song.performClick();

                break;


            case R.id.ll_next_song:

                endreach = false;
                System.out.println("" + position);
                songData.getSongs().size();

                if (local) {
                    if (localplaylist.isEmpty()) {
                        pausemusic();
                        Toast.makeText(MainActivity.this, "No song available", Toast.LENGTH_SHORT).show();
                    } else {

                        if ((Localplayposition + 1) >= localplaylist.size()) {
                            pausemusic();
                            ll_mini_progress.setVisibility(View.GONE);
                            ll_mini_play.setVisibility(View.VISIBLE);
                            ll_player.setVisibility(View.VISIBLE);
                            Toast.makeText(MainActivity.this, "No song available", Toast.LENGTH_SHORT).show();
                            endreach = true;
                        } else {
                            localservice(localplaylist.get(Localplayposition + 1), Localplayposition, true);
                            Localplayposition = Localplayposition + 1;
                        }
                    }
                } else {
                    if (endreach) {

                    } else {
                        ll_mini_progress.setVisibility(View.VISIBLE);
                        ll_mini_play.setVisibility(View.GONE);
                        ll_player.setVisibility(View.GONE);
                        temp_bar_visual.setVisibility(View.GONE);
                    }

                    handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            if (songData.getSongs().isEmpty()) {
                                ll_mini_play.setVisibility(View.VISIBLE);
                                ll_player.setVisibility(View.VISIBLE);
                                ll_mini_progress.setVisibility(View.GONE);
                                pausemusic();
                                Toast.makeText(MainActivity.this, "No song available", Toast.LENGTH_SHORT).show();
                            } else {

                                if ((position + 1) >= songData.getSongs().size()) {

                                    pausemusic();
                                    ll_mini_progress.setVisibility(View.GONE);
                                    ll_mini_play.setVisibility(View.VISIBLE);
                                    ll_player.setVisibility(View.VISIBLE);
                                    Toast.makeText(MainActivity.this, "No song available", Toast.LENGTH_SHORT).show();
                                    endreach = true;
                                } else {
                                    playmusiconline(songData.getSongs().get(position + 1), false);
                                    position = position + 1;
                                }
                            }
                        }
                    }, 5);
                } //the time is in milliseconds


                break;


            case R.id.ll_previous_song:

                endreach = false;

                if (local) {
                    if (localplaylist.isEmpty()) {
                        pausemusic();

                        ll_mini_progress.setVisibility(View.GONE);
                        ll_player.setVisibility(View.VISIBLE);
                        ll_mini_play.setVisibility(View.VISIBLE);
                        Toast.makeText(MainActivity.this, "No song available", Toast.LENGTH_SHORT).show();
                        start_time.setText("00:00");
                    } else {

                        if (Localplayposition == 0) {
                            pausemusic();
                            ll_mini_play.setVisibility(View.VISIBLE);
                            ll_player.setVisibility(View.VISIBLE);
                            ll_mini_progress.setVisibility(View.GONE);
                            Toast.makeText(MainActivity.this, "No song available", Toast.LENGTH_SHORT).show();
                            start_time.setText("00:00");
                        } else {
                            localservice(localplaylist.get(Localplayposition - 1), Localplayposition, true);
                            Localplayposition = Localplayposition - 1;
                        }
                    }
                } else {

                    ll_mini_progress.setVisibility(View.VISIBLE);
                    ll_mini_play.setVisibility(View.GONE);
                    ll_player.setVisibility(View.GONE);
                    temp_bar_visual.setVisibility(View.GONE);

                    handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            if (songData.getSongs().isEmpty()) {
                                pausemusic();

                                ll_mini_progress.setVisibility(View.GONE);
                                ll_player.setVisibility(View.VISIBLE);
                                ll_mini_play.setVisibility(View.VISIBLE);
                                Toast.makeText(MainActivity.this, "No song available", Toast.LENGTH_SHORT).show();
                                start_time.setText("00:00");
                            } else {

                                if (position == 0) {
                                    pausemusic();
                                    ll_mini_play.setVisibility(View.VISIBLE);
                                    ll_player.setVisibility(View.VISIBLE);
                                    ll_mini_progress.setVisibility(View.GONE);
                                    Toast.makeText(MainActivity.this, "No song available", Toast.LENGTH_SHORT).show();
                                    start_time.setText("00:00");
                                } else {
                                    playmusiconline(songData.getSongs().get(position - 1), false);
                                    position = position - 1;
                                }
                            }
                        }
                    }, 5);  //the time is in milliseconds
                }

                break;


            case R.id.but_max_view:

                runOnUiThread(new Runnable() {

                    public void run() {
                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
//                      LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(ll_web_parent.getWidth(), ll_web_parent.getHeight());
//                      llWeblayout.setLayoutParams(parms);
                    }
                });

                break;


            case R.id.but_min_view:

                runOnUiThread(new Runnable() {
                    public void run() {

                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                      LinearLayout.LayoutParams parmss = new LinearLayout.LayoutParams(ll_web_parent.getWidth(), ll_web_parent.getHeight() - ll_bottomSheetHeading.getHeight());
//                      llWeblayout.setLayoutParams(parmss);
                    }
                });

                break;


            case R.id.ll_mini_player_pause:

                card_pp.performClick();

                break;

            case R.id.ll_cross_mini:

                but_max_view.performClick();
                // bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

                if (isMyServiceRunning(NotificationService.class)) {
                    NotificationService.recmedia(mediaPlayer, MainActivity.this);
                    serviceIntent = new Intent(getApplicationContext(), NotificationService.class);
                    serviceIntent.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
                    startService(serviceIntent);
                }

                break;


            case R.id.img_music_mini:

                launchplayer();
                break;

            case R.id.card_pp:

                if (endreach && (currentPosition >= 29000)) {

                } else {
                    if (!mediaPlayer.isPlaying()) {
                        // Stopping
                        mediaPlayer.start();
                        img_pp.setImageDrawable(getResources().getDrawable(R.drawable.pause));
                        img_pp_mini.setImageDrawable(getResources().getDrawable(R.drawable.pause));

                        if (!isMyServiceRunning(NotificationService.class)) {
                            runync();
                        } else {
                            NotificationService.recmedia(mediaPlayer, MainActivity.this);
                            serviceIntent = new Intent(getApplicationContext(), NotificationService.class);
                            serviceIntent.setAction(Constants.ACTION.sync);
                            startService(serviceIntent);
                        }

                    } else {
                        // Playing
                        mediaPlayer.pause();
                        img_pp.setImageDrawable(getResources().getDrawable(R.drawable.play));
                        img_pp_mini.setImageDrawable(getResources().getDrawable(R.drawable.play));

                        if (!isMyServiceRunning(NotificationService.class)) {
                            runync();
                        } else {
                            NotificationService.recmedia(mediaPlayer, MainActivity.this);
                            serviceIntent = new Intent(getApplicationContext(), NotificationService.class);
                            serviceIntent.setAction(Constants.ACTION.sync);
                            startService(serviceIntent);
                        }
                    }
                }

                break;

            case R.id.ll_download:

                txt_mess_name.setText("Music Library");
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                ll_music_library.setVisibility(View.VISIBLE);
                llWeblayout.setVisibility(View.GONE);

                handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        AsyncTask.execute(new Runnable() {
                            @Override
                            public void run() {
                                mDb = AppDatabase.getInMemoryDatabase(getApplicationContext());
                                listUsers();
                            }
                        });
                    }
                }, 5);

                img_search.setImageResource(R.drawable.search_unselected);
                img_channel.setImageResource(R.drawable.channel_unselected);
                img_lib.setImageResource(R.drawable.lib_selected);
                img_menu.setImageResource(R.drawable.menu_unselected);
                img_user.setImageResource(R.drawable.user_unselected);
                img_home.setImageResource(R.drawable.home_unselect);

                img_search_1.setImageResource(R.drawable.search_unselected);
                img_channel_1.setImageResource(R.drawable.channel_unselected);
                img_lib_1.setImageResource(R.drawable.lib_selected);
                img_menu_1.setImageResource(R.drawable.menu_unselected);
                img_user_1.setImageResource(R.drawable.user_unselected);
                img_home_1.setImageResource(R.drawable.home_unselect);

                txt_home.setTextColor(getResources().getColor(R.color.icon_grey));
                txt_menu.setTextColor(getResources().getColor(R.color.icon_grey));
                txt_search.setTextColor(getResources().getColor(R.color.icon_grey));
                txt_channel.setTextColor(getResources().getColor(R.color.icon_grey));
                txt_library.setTextColor(getResources().getColor(R.color.icon_red));
                txt_profile.setTextColor(getResources().getColor(R.color.icon_grey));

                txt_home_1.setTextColor(getResources().getColor(R.color.icon_grey));
                txt_menu_1.setTextColor(getResources().getColor(R.color.icon_grey));
                txt_search_1.setTextColor(getResources().getColor(R.color.icon_grey));
                txt_channel_1.setTextColor(getResources().getColor(R.color.icon_grey));
                txt_library_1.setTextColor(getResources().getColor(R.color.icon_red));
                txt_profile_1.setTextColor(getResources().getColor(R.color.icon_grey));

//                Toast.makeText(this, "Downloading song", Toast.LENGTH_SHORT).show();
//                downloadFileplayer(songData.getSongs().get(position).getDownloadPathAbsolute(), DirectoryHelper.ROOT_DIRECTORY_NAME.concat("/"), songData.getSongs().get(position).getSongName(), songData.getSongs().get(position).getArtistName(), songData.getSongs().get(position).getImageSmSrc());

                break;

            case R.id.ll_min:

                but_min_view.performClick();
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!isMyServiceRunning(NotificationService.class)) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        } else {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }

    public void playprev() {

        ll_previous_song.performClick();
    }

    public void playnext() {
        ll_next_song.performClick();
    }

    @Override
    public void connectivityChanged(boolean isConnected) {

        if (isConnected) {
            //  message = "Good! Connected to Internet";
            System.out.println("Connected");
            webviewlaunch();

            MainActivity.this.runOnUiThread(new Runnable() {
                @TargetApi(Build.VERSION_CODES.KITKAT)
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                public void run() {
                    webView.evaluateJavascript("onUserLoggedIn(loggedInState:Boolean, user:UserObject)", new ValueCallback<String>() {
                        @Override
                        public void onReceiveValue(String s) {
                            // Do what you want with the return value
                            System.out.println(">>>>." + s);
                        }
                    });
                }
            });
        } else {
            //  message = "Sorry! Not connected to internet";
            System.out.println("not Connected");

            if (dialog.isShowing()) {

            } else {
                internetdialog();
            }
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    public void launchplayer() {
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    private void initViews() {
        View v = findViewById(R.id.bottomSheetLayout);
        v.setOnClickListener(null);
        v.setOnTouchListener(null);
        bottomSheetBehavior = BottomSheetBehavior.from(v);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }

    private void initListeners() {
        // Capturing the callbacks for bottom sheet
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(View bottomSheet, int newState) {
                bottomsheetstate = newState;
                // Check Logs to see how bottom sheets behaves
                switch (newState) {
                    case BottomSheetBehavior.STATE_COLLAPSED:

                        ll_bottomSheetHeading.setVisibility(View.VISIBLE);

                        card_prem_alert.setVisibility(View.GONE);

                        Log.e("Bottom Sheet Behaviour", "STATE_COLLAPSED");
                        break;

                    case BottomSheetBehavior.STATE_DRAGGING:

                        Log.e("Bottom Sheet Behaviour", "STATE_DRAGGING");
                        break;

                    case BottomSheetBehavior.STATE_EXPANDED:

                        ll_bottomSheetHeading.setVisibility(View.GONE);

                        if (!localplay) {
                            if (premiumuser) {
                                card_prem_alert.setVisibility(View.GONE);
                                // enableseekclick();
                            } else {
                                card_prem_alert.setVisibility(View.VISIBLE);
                                //disableseekclick();
                            }
                        }

                        Log.e("Bottom Sheet Behaviour", "STATE_EXPANDED");
                        break;

                    case BottomSheetBehavior.STATE_HIDDEN:

                        Log.e("Bottom Sheet Behaviour", "STATE_HIDDEN");
                        card_prem_alert.setVisibility(View.GONE);

                        if (!isMyServiceRunning(NotificationService.class)) {
                            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                        } else {
                            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        }

                        break;

                    case BottomSheetBehavior.STATE_SETTLING:
                        Log.e("Bottom Sheet Behaviour", "STATE_SETTLING");
                        break;
                }
            }

            @Override
            public void onSlide(View bottomSheet, float slideOffset) {

            }
        });
    }

    private boolean isMyServiceRunning(Class<NotificationService> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
//Syncronising the play pause next back i.e i notification  , actionbar,  etc 4 places..
    public void synchronizemain(String s) {
        decision = s;

        if (s.equals("Stop")) {
            mediaPlayer.stop();
            mediaPlayer.reset();
            mediaPlayer.release();

            mediaPlayer = new MediaPlayer();
            img_pp.setImageDrawable(getResources().getDrawable(R.drawable.play));
            img_pp_mini.setImageDrawable(getResources().getDrawable(R.drawable.play));

            runOnUiThread(new Runnable() {
                public void run() {
                    but_max_view.performClick();
                    // bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                }
            });

        } else if (s.equals("DONTHIDE")) {
            mediaPlayer.stop();
            mediaPlayer.reset();
            mediaPlayer.release();

            mediaPlayer = new MediaPlayer();
            img_pp.setImageDrawable(getResources().getDrawable(R.drawable.play));
            img_pp_mini.setImageDrawable(getResources().getDrawable(R.drawable.play));
        } else {
            if (mediaPlayer.isPlaying()) {
                // Stopping
                mediaPlayer.start();
                img_pp.setImageDrawable(getResources().getDrawable(R.drawable.pause));
                img_pp_mini.setImageDrawable(getResources().getDrawable(R.drawable.pause));

            } else {
                // Playing
                mediaPlayer.pause();
                img_pp.setImageDrawable(getResources().getDrawable(R.drawable.play));
                img_pp_mini.setImageDrawable(getResources().getDrawable(R.drawable.play));
            }
        }
    }


    public void runync() {
        NotificationService.recmedia(mediaPlayer, MainActivity.this);
        if (!isMyServiceRunning(NotificationService.class)) {
            serviceIntent = new Intent(getApplicationContext(), NotificationService.class);
            serviceIntent.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
            startService(serviceIntent);
        }
    }

    private Handler handlerrr = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            currentPosition = msg.what;
            // Update positionBar.
            positionBar.setProgress(currentPosition);

            if (!localplay) {
                if (!endreach) {
                    try {
                        if (!premiumuser) {
                            if (currentPosition >= 29000) {
                                ll_next_song.performClick();
                                if (bottomsheetstate == BottomSheetBehavior.STATE_COLLAPSED) {

                                    if (!remdiag.isShowing()) {
                                        remdiag.show();
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {

                    }
                }

            }

            // Update Labels.
            String elapsedTime = createTimeLabel(currentPosition);
            start_time.setText(elapsedTime);
        }
    };


    class WebAppInterface {
        Context context;

        public WebAppInterface(MainActivity mainActivity) {

            context = mainActivity;
        }

        @JavascriptInterface
        public void onPlay(final String song) {

            try {
                templocal_path = "";
                local = false;
                localplay = false;
                endreach = false;

                MainActivity.this.runOnUiThread(new Runnable() {
                    public void run() {

                        but_min_view.performClick();
                        ll_mini_progress.setVisibility(View.VISIBLE);
                        ll_mini_play.setVisibility(View.GONE);
                        ll_player.setVisibility(View.GONE);
                        temp_bar_visual.setVisibility(View.GONE);

                        // bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    }
                });

                JSONObject Jsonobject = new JSONObject(song);
                String jsonString = Jsonobject.toString();

                Gson gson = new Gson();
                songData = gson.fromJson(jsonString, SongData.class);


                for (int i = 0; i < songData.getSongs().size(); i++) {
                    if (Integer.parseInt("" + songData.getSongs().get(i).getMusicId()) == Integer.parseInt("" + songData.getSong().getMusicId())) {
                        playmusiconline(songData.getSongs().get(i), false);
                        position = i;
                        break;
                    }
                }
                runync();
            } catch (Exception e) {
                Log.d("json error", e.toString());
                onPlay(song);
            }
        }

        @JavascriptInterface
        public void onShare(final String title, final String text, final String url) {

            MainActivity.this.runOnUiThread(new Runnable() {
                @TargetApi(Build.VERSION_CODES.KITKAT)
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                public void run() {
                    Intent share = new Intent(android.content.Intent.ACTION_SEND);
                    share.setType("text/plain");
                    share.putExtra(Intent.EXTRA_SUBJECT, text);
                    share.putExtra(Intent.EXTRA_TEXT, url);
                    startActivity(Intent.createChooser(share, title));
                }
            });
        }

        @JavascriptInterface
        public void onUserLoggedInJson(final String jsonfile) {

            MainActivity.this.runOnUiThread(new Runnable() {
                public void run() {

                    JSONObject Jsonobject = null;
                    try {
                        Jsonobject = new JSONObject(jsonfile);
                        String jsonString = Jsonobject.toString();

                        Gson gson = new Gson();
                        Loggeddata loggeddata = gson.fromJson(jsonString, Loggeddata.class);

                        SIGNINState = loggeddata.getState();
                        System.out.println(">>>>>>>baba>>>>>>>>>>>" + jsonfile);
                    } catch (JSONException e) {
                        e.printStackTrace();


                    }

                }
            });
        }

        @JavascriptInterface
        public void onLoginGoogle() {
            MainActivity.this.runOnUiThread(new Runnable() {
                @TargetApi(Build.VERSION_CODES.KITKAT)
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                public void run() {
                    signIn();
                }
            });
        }

        /**
         * Show a toast from the web page
         */
        @JavascriptInterface
        public void onDownload(final String song) {

            try {
                JSONObject songDetails = new JSONObject(song);
                JSONObject songFile = new JSONObject(songDetails.getString("song"));
                String jsonString = songFile.toString();
                DownloadSong downloadSong = new DownloadSong();
                Gson gson = new Gson();
                downloadSong = gson.fromJson(jsonString, DownloadSong.class);
                String urlToDownload = downloadSong.getDownloadPathAbsolute();

                File path = myDirectory;
                file = new File(path, songFile.getString("songName") + ".mp4");
                //file = new File(MainActivity.this.getFilesDir(), songFile.getString("songName"));

                //Boolean success = file.createNewFile();


                if (checkStatus(MainActivity.this, DownloadManager.STATUS_RUNNING)) {
                    Toast.makeText(MainActivity.this, "Song is downloading please wait", Toast.LENGTH_SHORT).show();
                } else {
                    downloadFileplayer(urlToDownload, "/Android/data/" +
                            "" +
                            "com.bixbeat.android/", downloadSong.getSongName(), downloadSong.getArtistName(), downloadSong.getImageSmSrc());
                }
                // downloadFileplayer(songData.getSongs().get(position).getDownloadPathAbsolute(), DirectoryHelper.ROOT_DIRECTORY_NAME.concat("/"), songData.getSongs().get(position).getSongName(), songData.getSongs().get(position).getArtistName(), songData.getSongs().get(position).getImageSmSrc());

            } catch (Exception e) {
                Log.d("exception", "file not downloaded");
            }
        }
    }


    public void playmusiconline(final Song_ song_, boolean b) {
        premiumuser = false;
        MainActivity.this.runOnUiThread(new Runnable() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            public void run() {
                String hitdata = "{\"musicId\":" + "\"" + song_.getMusicId() + "\"" + ",\"preview\":" + "\"" + song_.getPreview() + "\"" + ",\"premium\":" + "\"" + song_.getPremium() + "\"" + ",\"siteId\":" + "\"" + song_.getSiteId() + "\"" + "}";
                webView.evaluateJavascript("javascript:window.Android.canPlayFull('" + hitdata + "')", new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        // Do what you want with the return value
                        System.out.println(">>>>." + s);

                        if (s.equals("true")) {
                            premiumuser = true;
                        } else {
                            premiumuser = false;
                        }
                    }
                });
            }
        });


        if (bottomsheetstate == BottomSheetBehavior.STATE_HIDDEN) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        localplay = false;
        local = false;
        endreach = false;

        if (b) {
            MainActivity.this.runOnUiThread(new Runnable() {
                public void run() {

                    ll_mini_progress.setVisibility(View.VISIBLE);
                    ll_mini_play.setVisibility(View.GONE);
                    ll_player.setVisibility(View.GONE);
                    temp_bar_visual.setVisibility(View.GONE);
                }
            });
        }

        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                MainActivity.this.runOnUiThread(new Runnable() {
                    public void run() {

                        if (mediaPlayer.isPlaying()) {
                            mediaPlayer.stop();
                        }

                        mediaPlayer = new MediaPlayer();
                        positionBar = (SeekBar) findViewById(R.id.songProgressBar);

                        runOnUiThread(new Runnable() {
                            public void run() {

                                ll_mini_progress.setVisibility(View.VISIBLE);
                                ll_mini_play.setVisibility(View.GONE);

                            }
                        });

                        try {
                            mediaPlayer.setDataSource("" + song_.getAbsoluteAudioPath());
                            mediaPlayer.prepare();
                            setlisteners();

                            finalTime = mediaPlayer.getDuration();
                            startTime = mediaPlayer.getCurrentPosition();
                            {
                                if (!endreach) {
                                    try {
                                        if (!premiumuser) {
                                            if (currentPosition >= 29000) {
                                                ll_next_song.performClick();
                                                if (bottomsheetstate == BottomSheetBehavior.STATE_COLLAPSED) {

                                                    if (!remdiag.isShowing()) {
                                                        remdiag.show();
                                                    }
                                                }
                                            }
                                        }
                                    } catch (Exception e) {

                                    }
                                }
                            }
                            positionBar.setMax((int) finalTime);
                            positionBar.setProgress((int) startTime);

                            String elapsedTime = createTimeLabel(finalTime);
                            end_time.setText(elapsedTime);

                            // attachseekbarlistener();

                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        runOnUiThread(new Runnable() {
                            public void run() {

                                txt_album_name_big.setText(song_.getArtistName());
                                txt_song_name_big.setText(song_.getSongName());

                                txt_mini_title.setText(song_.getSongName());
                                txt_mini_subtitle.setText(song_.getArtistName());

                                circleBarVisualizer = findViewById(R.id.visualizer);
                                temp_bar_visual = circleBarVisualizer;

                                // set custom color to the line.
                                circleBarVisualizer.setColor(ContextCompat.getColor(MainActivity.this, R.color.white));

                                // Set you media player to the visualizer.
                                try {
                                    circleBarVisualizer.setPlayer(mediaPlayer.getAudioSessionId());
                                } catch (Exception e) {

                                }

                                Picasso.get().load(song_.getImageSmSrc()).into(img_music_mini, new com.squareup.picasso.Callback() {
                                    @Override
                                    public void onSuccess() {

                                    }

                                    @Override
                                    public void onError(Exception e) {

                                        img_music_mini.setImageResource(R.drawable.bixlogo);
                                    }
                                });


                                Picasso.get().load(song_.getImageSmSrc()).into(album_small, new com.squareup.picasso.Callback() {
                                    @Override
                                    public void onSuccess() {

                                    }

                                    @Override
                                    public void onError(Exception e) {

                                        album_small.setImageResource(R.drawable.bixlogo);
                                    }
                                });

                                Picasso.get().load(song_.getImageSmSrc()).into(img_big, new com.squareup.picasso.Callback() {
                                    @Override
                                    public void onSuccess() {

                                    }

                                    @Override
                                    public void onError(Exception e) {

                                        img_big.setImageResource(R.drawable.bixlogo);
                                    }
                                });

                                senddata(song_.getImageSmSrc(), song_.getArtistName(), song_.getSongName());
                            }
                        });

                        mediaPlayer.start();
                        runync();
                    }
                });
            }
        }, 5);
    }

//mediaplayer listner
    public void setlisteners() {
        runOnUiThread(new Runnable() {
            public void run() {

                runOnUiThread(new Runnable() {
                    public void run() {
                        ll_player.setVisibility(View.GONE);
                        ll_mini_play.setVisibility(View.GONE);
                    }
                });

                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        // TODO Auto-generated method stub
                        mediaPlayer.stop();
                        mediaPlayer.reset();

                        runOnUiThread(new Runnable() {
                            public void run() {

                                ll_next_song.performClick();
                            }
                        });

                        Log.e("on completion :", "worked");
                    }
                });
                mediaPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                    @Override
                    public void onBufferingUpdate(MediaPlayer mp, int percent) {

                        Log.e("on bufferring update :", "worked > " + percent);
                    }
                });
                mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mediaPlayer) {

                        runOnUiThread(new Runnable() {
                            public void run() {
                                ll_mini_progress.setVisibility(View.GONE);
                                ll_mini_play.setVisibility(View.VISIBLE);
                                ll_player.setVisibility(View.VISIBLE);
                                temp_bar_visual.setVisibility(View.VISIBLE);
                                img_pp.setImageDrawable(getResources().getDrawable(R.drawable.pause));

                                serviceIntent = new Intent(getApplicationContext(), NotificationService.class);
                                serviceIntent.setAction(Constants.ACTION.setdata);
                                startService(serviceIntent);

                                playmusic();
                            }
                        });
                        Log.e("on prepared :", "worked");

                    }
                });

                mediaPlayer.setOnInfoListener(new MediaPlayer.OnInfoListener() {

                    @Override
                    public boolean onInfo(MediaPlayer mp, int what, int extra) {
                        switch (what) {
                            case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                                Log.d("BUFFERING_START", "onInfo() called with: mp = [" + mp.getCurrentPosition() + "], what = [" + what + "], extra = [" + extra + "]");
                                break;
                            case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                                Log.d("BUFFERING_END", "onInfo() called with: mp = [" + mp.getCurrentPosition() + "], what = [" + what + "], extra = [" + extra + "]");
                                break;
                        }
                        return true;
                    }
                });
            }
        });
    }

    public void playmusic() {
        // Stopping
        mediaPlayer.start();
        img_pp_mini.setImageDrawable(getResources().getDrawable(R.drawable.pause));
        img_pp.setImageDrawable(getResources().getDrawable(R.drawable.pause));

        if (!isMyServiceRunning(NotificationService.class)) {
            runync();
        } else {
            NotificationService.recmedia(mediaPlayer, MainActivity.this);
            serviceIntent = new Intent(getApplicationContext(), NotificationService.class);
            serviceIntent.setAction(Constants.ACTION.sync);
            startService(serviceIntent);
        }
    }


    public void pausemusic() {
        mediaPlayer.pause();
        img_pp.setImageDrawable(getResources().getDrawable(R.drawable.play));
        img_pp_mini.setImageDrawable(getResources().getDrawable(R.drawable.play));

        NotificationService.recmedia(mediaPlayer, MainActivity.this);
        serviceIntent = new Intent(getApplicationContext(), NotificationService.class);
        serviceIntent.setAction(Constants.ACTION.PausePlayer);
        startService(serviceIntent);
    }

    public void senddata(String imageSmSrc, String artistName, String songName) {
        NotificationService.notdata(imageSmSrc, artistName, songName);
    }

    @Override
    public void onBackPressed() {

        if (exit) {

            finish();
            ll_cross_mini.performClick();
            finishAffinity();
        } else {
            Toast.makeText(this, "Press Back again to Exit.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppDatabase.destroyInstance();
        unregisterReceiver(onDownloadComplete);
        unregisterReceiver(downloadReceiver);
        networkConnectionChecker.unregisterListener(this);
        ll_cross_mini.performClick();
        finish();
        finishAffinity();
    }


    public void downloadFileplayer(final String url, final String outputFile, final String songName, final String artistName, final String imageSmSrc) {

        final Thread t = new Thread(new Runnable() {
            public void run() {

                try {
                    temp_song_name = songName;
                    temp_artist_name = artistName;
                    temp_album_path = imageSmSrc;

                    File file = new File(Environment.getExternalStorageDirectory() + "/" + outputFile + "" + songName + ".mp4");

                    if (file.exists()) {
                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this, "Song already downloaded", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        Uri uri = Uri.parse("" + url);

                        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                        DownloadManager.Request request = new DownloadManager.Request(uri);
                        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);

                        // set title and description
                        request.setTitle(songName);
                        request.setDescription(artistName);

                        request.allowScanningByMediaScanner();
                        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

                        //set the local destination for download file to a path within the application's external files directory
                        request.setDestinationInExternalPublicDir(outputFile, songName + ".mp4");
                        temp_songpath = outputFile + "" + songName + ".mp4";

                        request.setMimeType("*/*");
                        downloadManager.enqueue(request);

                        downloadID = downloadManager.enqueue(request);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        t.start();
    }

    public static boolean checkStatus(Context context, int status) {
        DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadManager.Query query = new DownloadManager.Query();

        query.setFilterByStatus(status);
        Cursor c = downloadManager.query(query);
        if (c.moveToFirst()) {
            c.close();
            Log.i("DOWNLOAD_STATUS", String.valueOf(status));
            return true;
        }
        Log.i("AUTOMATION_DOWNLOAD", "DEFAULT");
        return false;
    }


    private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            // startActivity(new Intent(DownloadManager.ACTION_VIEW_DOWNLOADS));
         //   Toast.makeText(MainActivity.this, "letsee", Toast.LENGTH_SHORT).show();
        }
    };

    private BroadcastReceiver downloadclick = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            // startActivity(new Intent(DownloadManager.ACTION_VIEW_DOWNLOADS));
        //    Toast.makeText(MainActivity.this, "letsee", Toast.LENGTH_SHORT).show();
        }
    };

    private BroadcastReceiver onDownloadComplete = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //Fetching the download id received with the broadcast
            long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
            //Checking if the received broadcast is for our enqueued download by matching download id
            if (downloadID == id) {

                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        mDb = AppDatabase.getInMemoryDatabase(getApplicationContext());
                        //  mDb.userModel().deleteAll();

                        user.setSong_name(temp_song_name);
                        user.setArtist_name(temp_artist_name);
                        user.setImage_path(temp_album_path);
                        user.setSong_path(temp_songpath);
                        mDb.userModel().insertUser(user);
                    }
                });

                Toast.makeText(MainActivity.this, "Download Completed", Toast.LENGTH_SHORT).show();
                downloadManager.remove(downloadID);
            }
        }
    };

    private void listUsers() {

        users = mDb.userModel().loadAllUsers();
        Log.i("Room", "Number of Users: " + users.size());
        for (User user : users) {

            Log.i(TAG, "\nId: " + user.id + ", song name: " + user.getSong_name() + ", artist name: " + user.getArtist_name() + ", song id: " + user.getId() + ", song image path: " + user.getImage_path() + ", song file path: " + user.getSong_path());
        }

        localplaylist = new ArrayList<>();
        for (int i = 0; i < users.size(); i++) {
            String path = Environment.getExternalStorageDirectory() + "/" + users.get(i).getSong_path();

            File file = new File(path);

            if (file.exists()) {
                System.out.println("exist");
                localplaylist.add(users.get(i));
            } else {
                System.out.println("doesnt exist");
                mDb.userModel().deleteUser(users.get(i));
            }
        }
        users = mDb.userModel().loadAllUsers();

        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (users.isEmpty() || users.size() == 0) {
                    ll_empty_music_state.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);

                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                    ll_empty_music_state.setVisibility(View.INVISIBLE);
                    layoutManager = new LinearLayoutManager(MainActivity.this, RecyclerView.VERTICAL, false);
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    libraryAdapter = new LibraryAdapter(MainActivity.this, users);
                    recyclerView.setAdapter(libraryAdapter);
                }
            }
        });
    }

    public void play_local_music(User user, String localmusicpath) {

        mediaPlayer = new MediaPlayer();
        positionBar = (SeekBar) findViewById(R.id.songProgressBar);
        setlisteners();

        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            mediaPlayer.setDataSource(localmusicpath);
            mediaPlayer.prepare();

            finalTime = mediaPlayer.getDuration();
            startTime = mediaPlayer.getCurrentPosition();

            positionBar.setMax((int) finalTime);
            positionBar.setProgress((int) startTime);

            String elapsedTime = createTimeLabel(finalTime);
            end_time.setText(elapsedTime);

        } catch (IOException e) {
            e.printStackTrace();
        }

        txt_album_name_big.setText(user.getArtist_name());
        txt_song_name_big.setText(user.getSong_name());

        txt_mini_title.setText(user.getSong_name());
        txt_mini_subtitle.setText(user.getArtist_name());

        img_pp_mini.setImageDrawable(getResources().getDrawable(R.drawable.pause));
        img_pp.setImageDrawable(getResources().getDrawable(R.drawable.pause));

        circleBarVisualizer = findViewById(R.id.visualizer);
        temp_bar_visual = circleBarVisualizer;

        // set custom color to the line.
        circleBarVisualizer.setColor(ContextCompat.getColor(MainActivity.this, R.color.white));

        // Set you media player to the visualizer.
        circleBarVisualizer.setPlayer(mediaPlayer.getAudioSessionId());

        Picasso.get().load(user.getImage_path()).into(img_music_mini, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {

                img_music_mini.setImageResource(R.drawable.bixlogo);
            }
        });


        Picasso.get().load(user.getImage_path()).into(album_small, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {

                album_small.setImageResource(R.drawable.bixlogo);
            }
        });

        Picasso.get().load(user.getImage_path()).into(img_big, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {

                img_big.setImageResource(R.drawable.bixlogo);
            }
        });

        senddata(user.getImage_path(), user.getArtist_name(), user.getSong_name());

        mediaPlayer.start();
        runync();

        local = true;
    }

    //to play local mmusic
    public void localservice(User user, int position, boolean local) {
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        String path = Environment.getExternalStorageDirectory() + "/" + user.getSong_path();

//        File file = new File(path);
//        if(file.exists())
//        {
//            System.out.println("exist");
//        }
//        else
//        {
//            System.out.println("doesnt exist");
//        }

        localplay = local;

        if (isMyServiceRunning(NotificationService.class)) {

            if (mediaPlayer.isPlaying()) {
                checkpause(user, path);
            } else {
                checkplay(user, path);
            }
        } else {
            play_local_music(user, path);
        }

        templocal_path = path;
        Localplayposition = position;
    }

    public void checkpause(User user, String path) {
        if (templocal_path == null || templocal_path.equals("") || templocal_path.isEmpty()) {
            pausemusic();

            NotificationService.reclocalobject(user, path);

            NotificationService.recmedia(mediaPlayer, MainActivity.this);
            serviceIntent = new Intent(getApplicationContext(), NotificationService.class);
            serviceIntent.setAction(Constants.ACTION.STOPFORLOCAL);
            startService(serviceIntent);
        } else if (templocal_path.equals(path)) {
            pausemusic();
        } else {
            pausemusic();

            NotificationService.reclocalobject(user, path);

            NotificationService.recmedia(mediaPlayer, MainActivity.this);
            serviceIntent = new Intent(getApplicationContext(), NotificationService.class);
            serviceIntent.setAction(Constants.ACTION.STOPFORLOCAL);
            startService(serviceIntent);
        }
    }

    public void checkplay(User user, String path) {
        if (templocal_path == null || templocal_path.equals("") || templocal_path.isEmpty()) {
            NotificationService.reclocalobject(user, path);

            NotificationService.recmedia(mediaPlayer, MainActivity.this);
            serviceIntent = new Intent(getApplicationContext(), NotificationService.class);
            serviceIntent.setAction(Constants.ACTION.STOPFORLOCAL);
            startService(serviceIntent);
        } else if (templocal_path.equals(path)) {
            playmusic();
        } else {
            NotificationService.reclocalobject(user, path);

            NotificationService.recmedia(mediaPlayer, MainActivity.this);
            serviceIntent = new Intent(getApplicationContext(), NotificationService.class);
            serviceIntent.setAction(Constants.ACTION.STOPFORLOCAL);
            startService(serviceIntent);
        }
    }


    public void internetdialog() {
        Button stay = (Button) dialog.findViewById(R.id.but_ok_email);
        Button but_local_songs = (Button) dialog.findViewById(R.id.but_local_songs);

        stay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();

                if (!isOnline(MainActivity.this)) {
                    internetdialog();
                    //no_int_song=true;
                }
            }
        });

        but_local_songs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                ll_my_lib.performClick();
            }
        });

        if (dialog.isShowing()) {

        } else {
            dialog.show();
        }
    }

    public void webviewlaunch() {
        String url = "https://bixbeat.com/?platform=android";
        loadurll(url);
    }


    public boolean isOnline(Context context) {
        boolean isOnline = false;
        try {
            ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = connManager.getActiveNetworkInfo();

            isOnline = (netInfo != null && netInfo.isConnected());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return isOnline;
    }

    public void attachseekbarlistener() {
        positionBar.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if (fromUser) {
                            mediaPlayer.seekTo(progress);
                            positionBar.setProgress(progress);
                            //positionBar.setMax(finalTime);
                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                }
        );
    }

//    public void disableseekclick()
//    {
//        positionBar.setOnTouchListener(new View.OnTouchListener(){
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                return false;
//            }
//        });
//    }
//
//    public void enableseekclick()
//    {
//        positionBar.setOnTouchListener(new View.OnTouchListener(){
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                return true;
//            }
//        });
//    }

    public void paymentlaunch() {
        String url = "https://bixbeat.com/premium/?platform=android";
        loadurll(url);
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        //  displayAlert(MainActivity.this, "Something went wrong! Please try again!");
    }

    public void facebook_login() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                System.out.println("onSuccess");
                String accessToken = loginResult.getAccessToken().getToken();
                Log.i("accessToken", accessToken);

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.i("LoginActivity", response.toString());
                        // Get facebook data from login
                        String jsonString = object.toString();
                        gson = new Gson();
                        Facebookdata facedata = gson.fromJson(jsonString, Facebookdata.class);

                        if (facedata.getEmail() == null || facedata.getEmail().equals("")) {
                            //showCustomDialog();
                        } else {

                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, location"); // Par�metros que pedimos a facebook
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Toast.makeText(MainActivity.this, "unable to ", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(MainActivity.this, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                // displayAlert(MainActivity.this, "" + error.getMessage());
            }
        });
    }

    private void senddatatoweb(Task<GoogleSignInAccount> task) {
        try {
            GoogleSignInAccount account = task.getResult(ApiException.class);
            final String authCode = account.getServerAuthCode();
            // webView.loadUrl("javascript:window.Android.onLoginGoogleDone('" + authCode + "')");

            MainActivity.this.runOnUiThread(new Runnable() {
                @TargetApi(Build.VERSION_CODES.KITKAT)
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                public void run() {
                    webView.evaluateJavascript("javascript:window.Android.onLoginGoogleDone('" + authCode + "')", new ValueCallback<String>() {
                        @Override
                        public void onReceiveValue(String s) {
                            // Do what you want with the return value
                            System.out.println(">>>>." + s);
                        }
                    });
                }
            });
        } catch (ApiException e) {

        }
    }


    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            result.getSignInAccount().getServerAuthCode();
            result.getSignInAccount().getIdToken();
            final String authCode = acct.getServerAuthCode();

            MainActivity.this.runOnUiThread(new Runnable() {
                @TargetApi(Build.VERSION_CODES.KITKAT)
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                public void run() {
                    webView.evaluateJavascript("javascript:window.Android.onLoginGoogleDone('" + authCode + "')", new ValueCallback<String>() {
                        @Override
                        public void onReceiveValue(String s) {
                            // Do what you want with the return value
                            System.out.println(">>>>." + s);
                        }
                    });
                }
            });

//            Log.e(TAG, "display name: " + acct.getDisplayName());
//            String personName = "";
//
//            result.getSignInAccount().getGivenName();
//            result.getSignInAccount().getDisplayName();
//            result.getSignInAccount().getAccount();
//
//            try {
//                if (acct.getDisplayName() == null) {
//                    personName = result.getSignInAccount().getGivenName();
//                } else {
//            personName = result.getSignInAccount().getDisplayName();
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//
//            }
//
//            if( personName == null || personName.equals(""))
//            {
//
//                handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//
//                        signIn();
//
//                    }
//                }, 2000);
//            }
//            else
//            {
//                boolean found = false;
//                String search  = "@";
//
//                if ( personName.toLowerCase().indexOf(search.toLowerCase()) != -1 ) {
//
//                    System.out.println("I found the keyword");
//                    found=true;
//
//                } else {
//
//                    System.out.println("not found");
//                    found= false;
//                }
//
//                if(found)
//                {
//                    signIn();
//                }
//                else
//                {
//                    String email = acct.getEmail();
//                    String firstname="", lastname = "";
//                    try {
//                        String[] aSplit = personName.split(" ");
//                        firstname = aSplit[0];
//                        // now for the rest of string we can just append the rest
//                        for (int i = 1; i < aSplit.length; i++) {
//                            lastname += aSplit[i];
//                            lastname += " ";
//                        }
//                        System.out.println("First word = [" + firstname + "]");
//                        System.out.println("Rest of string = [" + lastname + "]");
//
//                    }catch (Exception e) {
//                        e.printStackTrace();
//                        //displayAlert(MainActivity.this, "Something went wrong! Please try again!");
//                    }
//                    Log.e(TAG, "Name: " + personName + ", email: " + email);
//                }
//            }

        } else {
            // Signed out, show unauthenticated UI.
            // load_dialog.cancel();
            Toast.makeText(MainActivity.this, "Already Registered with this account try another. ", Toast.LENGTH_SHORT).show();
        }
    }

    public void disconnectFromFacebook() {

        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }

        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {

                LoginManager.getInstance().logOut();

            }
        }).executeAsync();
    }

    public void showmenu() {
        //dialog intialization
        menudiag = new Dialog(MainActivity.this);
        menudiag.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        menudiag.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        menudiag.setContentView(R.layout.menu_data);
        menudiag.setCancelable(true);

        ll_dj_mixes = (LinearLayout) menudiag.findViewById(R.id.ll_dj_mixes);
        ll_term = (LinearLayout) menudiag.findViewById(R.id.ll_term);
        ll_about = (LinearLayout) menudiag.findViewById(R.id.ll_about);
        ll_record_label = (LinearLayout) menudiag.findViewById(R.id.ll_record_label);

        LinearLayout ll_privacy = (LinearLayout) menudiag.findViewById(R.id.ll_privacy);
        LinearLayout ll_insta = (LinearLayout) menudiag.findViewById(R.id.ll_insta);
        LinearLayout ll_facebook = (LinearLayout) menudiag.findViewById(R.id.ll_facebook);
        LinearLayout ll_twitter = (LinearLayout) menudiag.findViewById(R.id.ll_twitter);
        LinearLayout ll_signin_user = (LinearLayout) menudiag.findViewById(R.id.ll_signin_user);
        LinearLayout ll_signout_user = (LinearLayout) menudiag.findViewById(R.id.ll_signout_user);

        LinearLayout ll_advertise = (LinearLayout) menudiag.findViewById(R.id.ll_advertise);
        LinearLayout ll_contacts = (LinearLayout) menudiag.findViewById(R.id.ll_contacts);
        LinearLayout ll_upload = (LinearLayout) menudiag.findViewById(R.id.ll_upload);

        if(SIGNINState)
        {
            ll_signin_user.setVisibility(View.GONE);
            ll_signout_user.setVisibility(View.VISIBLE);
        }
        else
        {
            ll_signout_user.setVisibility(View.GONE);
            ll_signin_user.setVisibility(View.VISIBLE);
        }

        ll_dj_mixes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menudiag.cancel();
                loadurll("https://bixbeat.com/dj/?platform=android");
            }
        });


        ll_signin_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menudiag.cancel();
                loadurll("https://bixbeat.com/dj/?platform=android");
                MainActivity.this.runOnUiThread(new Runnable() {
                    @TargetApi(Build.VERSION_CODES.KITKAT)
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)

                    public void run() {
                        webView.evaluateJavascript("javascript:window.Android.onLogin()", new ValueCallback<String>() {
                            @Override
                            public void onReceiveValue(String s) {
                                // Do what you want with the return value   window["Android"].onLogout() //window["Android"].onUserLoggedIn(state, userLocal);

                                Log.e("Loginning",s);
                                ll_home.performClick();
                            }
                        });
                    }
                });
            }
        });

        ll_signout_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menudiag.cancel();
//                loadurll("https://bixbeat.com/profile/?platform=android");

                MainActivity.this.runOnUiThread(new Runnable() {
                    @TargetApi(Build.VERSION_CODES.KITKAT)
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    public void run() {
                        webView.evaluateJavascript("javascript:window.Android.onLogout()", new ValueCallback<String>() {
                            @Override
                            public void onReceiveValue(String s) {
                                // Do what you want with the return value   window["Android"].onLogout() //window["Android"].onUserLoggedIn(state, userLocal);
                                Toast.makeText(MainActivity.this, "Logout Successfully", Toast.LENGTH_SHORT).show();
                                ll_home.performClick();
                            }
                        });
                    }
                });
            }
        });

        ll_term.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menudiag.cancel();
                loadurll("https://bixbeat.com/terms/?platform=android");
            }
        });

        ll_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menudiag.cancel();
                loadurll("https://bixbeat.com/about/?platform=android");
            }
        });

        ll_record_label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menudiag.cancel();
                loadurll("https://bixbeat.com/labels/?platform=android");
            }
        });

        ll_dj_mixes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menudiag.cancel();
                loadurll("https://bixbeat.com/dj/?platform=android");
            }
        });

        ll_privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menudiag.cancel();
                loadurll("https://bixbeat.com/privacy/?platform=android");
            }
        });

        ll_insta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menudiag.cancel();
                loadurll("https://www.instagram.com/bixbeat/?platform=android");
            }
        });

        ll_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menudiag.cancel();
                loadurll("https://www.facebook.com/BixBEATMusic/?platform=android");
            }
        });

        ll_twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menudiag.cancel();
                loadurll("https://twitter.com/bixbeat/?platform=android");
            }
        });

        ll_contacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menudiag.cancel();
                loadurll("https://bixbeat.com/contact/?platform=android");
            }
        });

        ll_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menudiag.cancel();
                loadurll("https://bixbeat.com/upload/?platform=android");
            }
        });

        ll_advertise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menudiag.cancel();
                loadurll("https://bixbeat.com/advertise/?platform=android");
            }
        });

        menudiag.show();
    }

    public void loadurll(String s) {
        String url = "" + s;
        webView.loadUrl(url);
    }

    public void loadfunurll(String s) {
        String url = "" + s;
        webView.loadUrl("javascript:window.Android.onNavigateByUrl("+url+")");
    }
}